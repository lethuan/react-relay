import React, { Component } from 'react'
import { QueryRenderer, graphql } from 'react-relay';
import env from '../../configs/Environment'
import ListUser from './ListUser'
const queries = graphql`
  query UserRootQuery{
      allUsers{
        id
      }
  }
`
class UserRoot extends Component {
    render() {
        return (
            <QueryRenderer
                query={queries}
                environment={env}
                render={({ error, props }) => {
                    if (error)
                        return <div>{error.message}</div>
                    else if (props)
                        return <ListUser users={props} />
                    else
                        return <div>Loading...</div>
                }}
            />
        )
    }
}
export default UserRoot
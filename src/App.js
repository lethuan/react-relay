import React, { Component } from 'react';
import { Switch, Route } from 'react-router-dom'
import UserRoot from './screens/user/UserRoot'
import UserComponents from './screens/user/user'

class App extends Component {
  render() {
    return (
      <Switch>
        <Route exact path ='/' component = {UserRoot}/>
        <Route exact path ='/home' component = {UserComponents}/>
      </Switch>
    );
  }
}

export default App;
